﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class vdifficulty : MonoBehaviour {

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void Easy()
    {
        SceneManager.LoadScene("veasy");
    }
    public void Medium()
    {
        SceneManager.LoadScene("vmedium");
    }
    public void Hard()
    {
        SceneManager.LoadScene("vhard");
    }
}
