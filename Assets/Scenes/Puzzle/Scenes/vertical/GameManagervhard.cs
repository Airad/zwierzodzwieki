﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameManagervhard : MonoBehaviour {


    public const string DRAGGABLE_TAG = "UIDraggable";

    private bool dragging = false;

    private Vector2 originalPosition;

    private Vector3 firstPosition;
    private Vector3 secondPosition;
    private Vector3 thirdPosition;
    private Vector3 forthPosition;

    private Transform objectToDrag;
    private Image objectToDragImage;

    List<RaycastResult> hitObject = new List<RaycastResult>();
    private AudioSource SoundSource;
    public AudioSource MusicSource;
    public AudioClip command;
    public AudioClip good;

    public Puzzle[] puzzles;
    private Puzzle current;
    private Puzzle random;
    private static List<int> image;
    private static List<Puzzle> unsolvedPuzzles;
    [SerializeField]
    private float timeBTWQuestions = 1f;
    [SerializeField]
    public Image first;
    [SerializeField]
    public Image second;
    [SerializeField]
    public Image third;
    [SerializeField]
    public Image forth;
    #region MonoBehaviour API

    void Start()
    {
        SoundSource = GetComponent<AudioSource>();
        if (unsolvedPuzzles == null || unsolvedPuzzles.Count == 0)
        {
            unsolvedPuzzles = puzzles.ToList<Puzzle>();
        }
        //Debug.Log(image.Count);
        firstPosition = first.transform.position;
        secondPosition = second.transform.position;
        thirdPosition = third.transform.position;
        forthPosition = forth.transform.position;
        //Debug.Log(firstPosition.x+" "+ firstPosition.y + "/n"+ first.transform.position.x + " " + first.transform.position.y);
        image = new List<int> { 0, 1, 2, 3 };
        SetCurrentPuzzle();
        SoundSource.clip = command;
        SoundSource.Play();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            objectToDrag = GetDraggableTransformUnderMouse();
            if (objectToDrag != null)
            {
                dragging = true;

                objectToDrag.SetAsLastSibling();

                originalPosition = objectToDrag.position;
                objectToDragImage = objectToDrag.GetComponent<Image>();
                objectToDragImage.raycastTarget = false;
            }
        }

        if (dragging)
        {
            objectToDrag.position = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (objectToDrag != null)
            {
                Transform objectToReplace = GetDraggableTransformUnderMouse();

                if (objectToReplace != null)
                {
                    objectToDrag.position = objectToReplace.position;
                    objectToReplace.position = originalPosition;
                }
                else
                {
                    objectToDrag.position = originalPosition;
                }
                objectToDragImage.raycastTarget = true;
                objectToDrag = null;
            }
            dragging = false;
            if (GameObject.Find("0").transform.position == firstPosition)
                if (GameObject.Find("1").transform.position == secondPosition)
                    if (GameObject.Find("2").transform.position == thirdPosition)
                        if (GameObject.Find("3").transform.position == forthPosition)
                        {
                            Debug.Log("Success!");
                            StartCoroutine(TransisiontoNext());
                        }

        }


    }
    IEnumerator TransisiontoNext()
    {
        

        MusicSource.Stop();
        SoundSource.clip = good;
        SoundSource.Play();
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        SoundSource.clip = current.sound;
        SoundSource.Play();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        yield return new WaitForSeconds(timeBTWQuestions);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void SetCurrentPuzzle()
    {
        int randomPuzzleIndex = Random.Range(0, unsolvedPuzzles.Count);
        current = unsolvedPuzzles[randomPuzzleIndex];
        unsolvedPuzzles.Remove(current);
        random = current;
        int i = 0;
        while (random.image.Count > 0)
        {
            int randomImageIndex = Random.Range(0, random.image.Count);

            //Debug.Log(image[i] + " " + random.image[randomImageIndex]);
            if (i == 0)
            {
                first.sprite = random.image[randomImageIndex];
                first.name = image[randomImageIndex].ToString();
            }
            if (i == 1)
            {
                second.sprite = random.image[randomImageIndex];
                second.name = image[randomImageIndex].ToString();
            }
            if (i == 2)
            {
                third.sprite = random.image[randomImageIndex];
                third.name = image[randomImageIndex].ToString();
            }
            if (i == 3)
            {
                forth.sprite = random.image[randomImageIndex];
                forth.name = image[randomImageIndex].ToString();
            }
            random.image.RemoveAt(randomImageIndex);
            image.RemoveAt(randomImageIndex);
            i++;
        }
        /*while (current.image.Count > 0)
        {
            int randomImageIndex = Random.Range(0, random.image.Count);
            int randomButtonIndex = Random.Range(0, image.Count);
            Debug.Log(image[randomButtonIndex] + " " + random.image[randomImageIndex]);
            if (image[randomButtonIndex] == 0)
            {
                first.sprite = random.image[randomImageIndex];
                first.name = image[randomButtonIndex].ToString();
            }
            if (image[randomButtonIndex] == 1)
            {
                second.sprite = random.image[randomImageIndex];
                second.name = image[randomButtonIndex].ToString();
            }
            if (image[randomButtonIndex] == 2)
            {
                third.sprite = random.image[randomImageIndex];
                third.name = image[randomButtonIndex].ToString();
            }
            if (image[randomButtonIndex] == 3)
            {
                forth.sprite = random.image[randomImageIndex];
                forth.name = image[randomButtonIndex].ToString();
            }

            random.image.RemoveAt(randomImageIndex);
            
            image.RemoveAt(randomImageIndex);
        }*/
    }
    #endregion

    private GameObject GetObjectUnderMouse()
    {
        var pointer = new PointerEventData(EventSystem.current);

        pointer.position = Input.mousePosition;

        EventSystem.current.RaycastAll(pointer, hitObject);

        if (hitObject.Count <= 0) return null;

        return hitObject.First().gameObject;
    }
    private Transform GetDraggableTransformUnderMouse()
    {
        GameObject clicked = GetObjectUnderMouse();

        if (clicked != null && clicked.tag == DRAGGABLE_TAG)
        {
            return clicked.transform;
        }
        return null;
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
