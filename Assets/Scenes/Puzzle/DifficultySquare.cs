﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DifficultySquare : MonoBehaviour {

    public void Easy()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void Medium()
    {
        SceneManager.LoadScene("Medium");
    }
    public void Hard()
    {
        SceneManager.LoadScene("Hard");
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
