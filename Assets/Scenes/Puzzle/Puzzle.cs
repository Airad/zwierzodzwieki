﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Puzzle
{
    public List<Sprite> image;
    public AudioClip sound;
    public string animal;
    public Sprite wholeImage;
}

