﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class GameManagerHard : MonoBehaviour {


    public const string DRAGGABLE_TAG = "UIDraggable";

    private bool dragging = false;

    private Vector2 originalPosition;
    private List<Vector3> imagepositions;

    private Transform objectToDrag;
    private Image objectToDragImage;

    List<RaycastResult> hitObject = new List<RaycastResult>();
    private AudioSource SoundSource;
    public AudioSource MusicSource;
    public AudioClip command;
    public AudioClip good;

    public Puzzle[] puzzles;
    private Puzzle current;
    private Puzzle random;
    private static List<int> image;
    private static List<Puzzle> unsolvedPuzzles;
    [SerializeField]
    private float timeBTWQuestions = 1f;
    [SerializeField]
    public Image first;
    [SerializeField]
    public Image second;
    [SerializeField]
    public Image third;
    [SerializeField]
    public Image forth;
    [SerializeField]
    public Image fifth;
    [SerializeField]
    public Image sixth;
    [SerializeField]
    public Image seventh;
    [SerializeField]
    public Image eigth;
    [SerializeField]
    public Image ninth;
    [SerializeField]
    public Image ten;
    [SerializeField]
    public Image eleven;
    [SerializeField]
    public Image twelve;
    [SerializeField]
    public Image thirteen;
    [SerializeField]
    public Image fourteen;
    [SerializeField]
    public Image fifteen;
    [SerializeField]
    public Image sixteen;
    #region MonoBehaviour API

    void Start()
    {
        SoundSource = GetComponent<AudioSource>();
        if (unsolvedPuzzles == null || unsolvedPuzzles.Count == 0)
        {
            unsolvedPuzzles = puzzles.ToList<Puzzle>();
        }
        imagepositions=new List<Vector3>();
        //Debug.Log(image.Count);
        imagepositions.Add(first.transform.position);
        imagepositions.Add(second.transform.position);
        imagepositions.Add(third.transform.position);
        imagepositions.Add(forth.transform.position);
        imagepositions.Add(fifth.transform.position);
        imagepositions.Add(sixth.transform.position);
        imagepositions.Add(seventh.transform.position);
        imagepositions.Add(eigth.transform.position);
        imagepositions.Add(ninth.transform.position);
        imagepositions.Add(ten.transform.position);
        imagepositions.Add(eleven.transform.position);
        imagepositions.Add(twelve.transform.position);
        imagepositions.Add(thirteen.transform.position);
        imagepositions.Add(fourteen.transform.position);
        imagepositions.Add(fifteen.transform.position);
        imagepositions.Add(sixteen.transform.position);

        //Debug.Log(firstPosition.x+" "+ firstPosition.y + "/n"+ first.transform.position.x + " " + first.transform.position.y);
        image = new List<int> { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
        SetCurrentPuzzle();
        SoundSource.clip = command;
        SoundSource.Play();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            objectToDrag = GetDraggableTransformUnderMouse();
            if (objectToDrag != null)
            {
                dragging = true;

                objectToDrag.SetAsLastSibling();

                originalPosition = objectToDrag.position;
                objectToDragImage = objectToDrag.GetComponent<Image>();
                objectToDragImage.raycastTarget = false;
            }
        }

        if (dragging)
        {
            objectToDrag.position = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (objectToDrag != null)
            {
                Transform objectToReplace = GetDraggableTransformUnderMouse();

                if (objectToReplace != null)
                {
                    objectToDrag.position = objectToReplace.position;
                    objectToReplace.position = originalPosition;
                }
                else
                {
                    objectToDrag.position = originalPosition;
                }
                objectToDragImage.raycastTarget = true;
                objectToDrag = null;
            }
            dragging = false;
            if (GameObject.Find("0").transform.position == imagepositions[0])
                if (GameObject.Find("1").transform.position == imagepositions[1])
                    if (GameObject.Find("2").transform.position == imagepositions[2])
                        if (GameObject.Find("3").transform.position == imagepositions[3])
                            if (GameObject.Find("4").transform.position == imagepositions[4])
                                if (GameObject.Find("5").transform.position == imagepositions[5])
                                    if (GameObject.Find("6").transform.position == imagepositions[6])
                                        if (GameObject.Find("7").transform.position == imagepositions[7])
                                            if (GameObject.Find("8").transform.position == imagepositions[8])
                                                if (GameObject.Find("9").transform.position == imagepositions[9])
                                                    if (GameObject.Find("10").transform.position == imagepositions[10])
                                                        if (GameObject.Find("11").transform.position == imagepositions[11])
                                                            if (GameObject.Find("12").transform.position == imagepositions[12])
                                                                if (GameObject.Find("13").transform.position == imagepositions[13])
                                                                    if (GameObject.Find("14").transform.position == imagepositions[14])
                                                                        if (GameObject.Find("15").transform.position == imagepositions[15])
                                                                        {
                                                                            Debug.Log("Success!");
                                                                            StartCoroutine(TransisiontoNext());
                                                                        }
        }


    }
    IEnumerator TransisiontoNext()
    {
        unsolvedPuzzles.Remove(current);

        MusicSource.Stop();
        SoundSource.clip = good;
        SoundSource.Play();
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        SoundSource.clip = current.sound;
        SoundSource.Play();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        yield return new WaitForSeconds(timeBTWQuestions);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void SetCurrentPuzzle()
    {
        int randomPuzzleIndex = Random.Range(0, unsolvedPuzzles.Count);
        current = unsolvedPuzzles[randomPuzzleIndex];
        random = current;
        int i = 0;
        while (random.image.Count > 0)
        {
            int randomImageIndex = Random.Range(0, random.image.Count);

            //Debug.Log(image[i] + " " + random.image[randomImageIndex]);
            if (i == 0)
            {
                first.sprite = random.image[randomImageIndex];
                first.name = image[randomImageIndex].ToString();
            }
            if (i == 1)
            {
                second.sprite = random.image[randomImageIndex];
                second.name = image[randomImageIndex].ToString();
            }
            if (i == 2)
            {
                third.sprite = random.image[randomImageIndex];
                third.name = image[randomImageIndex].ToString();
            }
            if (i == 3)
            {
                forth.sprite = random.image[randomImageIndex];
                forth.name = image[randomImageIndex].ToString();
            }
            if (i == 4)
            {
                fifth.sprite = random.image[randomImageIndex];
                fifth.name = image[randomImageIndex].ToString();
            }
            if (i == 5)
            {
                sixth.sprite = random.image[randomImageIndex];
                sixth.name = image[randomImageIndex].ToString();
            }
            if (i == 6)
            {
                seventh.sprite = random.image[randomImageIndex];
                seventh.name = image[randomImageIndex].ToString();
            }
            if (i == 7)
            {
                eigth.sprite = random.image[randomImageIndex];
                eigth.name = image[randomImageIndex].ToString();
            }
            if (i == 8)
            {
                ninth.sprite = random.image[randomImageIndex];
                ninth.name = image[randomImageIndex].ToString();
            }
            if (i == 9)
            {
                ten.sprite = random.image[randomImageIndex];
                ten.name = image[randomImageIndex].ToString();
            }
            if (i == 10)
            {
                eleven.sprite = random.image[randomImageIndex];
                eleven.name = image[randomImageIndex].ToString();
            }
            if (i == 11)
            {
                twelve.sprite = random.image[randomImageIndex];
                twelve.name = image[randomImageIndex].ToString();
            }
            if (i == 12)
            {
                thirteen.sprite = random.image[randomImageIndex];
                thirteen.name = image[randomImageIndex].ToString();
            }
            if (i == 13)
            {
                fourteen.sprite = random.image[randomImageIndex];
                fourteen.name = image[randomImageIndex].ToString();
            }
            if (i == 14)
            {
                fifteen.sprite = random.image[randomImageIndex];
                fifteen.name = image[randomImageIndex].ToString();
            }
            if (i == 15)
            {
                sixteen.sprite = random.image[randomImageIndex];
                sixteen.name = image[randomImageIndex].ToString();
            }
            random.image.RemoveAt(randomImageIndex);
            image.RemoveAt(randomImageIndex);
            i++;
        }
    }
    #endregion

    private GameObject GetObjectUnderMouse()
    {
        var pointer = new PointerEventData(EventSystem.current);

        pointer.position = Input.mousePosition;

        EventSystem.current.RaycastAll(pointer, hitObject);

        if (hitObject.Count <= 0) return null;

        return hitObject.First().gameObject;
    }
    private Transform GetDraggableTransformUnderMouse()
    {
        GameObject clicked = GetObjectUnderMouse();

        if (clicked != null && clicked.tag == DRAGGABLE_TAG)
        {
            return clicked.transform;
        }
        return null;
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
