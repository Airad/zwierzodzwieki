﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionToNExt : MonoBehaviour
{
    public void ToFarm()
    {
        SceneManager.LoadScene("SampleScene");
    }
    public void ToJungle()
    {
        SceneManager.LoadScene("Jungle");
    }
    public void ToForest()
    {
        SceneManager.LoadScene("Forest");
    }
}
