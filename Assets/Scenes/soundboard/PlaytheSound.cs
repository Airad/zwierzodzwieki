﻿using UnityEngine;

public class PlaytheSound : MonoBehaviour
{
    public AudioSource sound;
    private bool isPlaying = false;

    public void PlaySound()
    {
        if (isPlaying)
        {
            Debug.Log(isPlaying);
            sound.Stop();
            isPlaying = false;
        }
        else
        {
            Debug.Log(isPlaying);
            sound.Play();
            isPlaying = true;
        }
    }
}

