﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Difficulty : MonoBehaviour {

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void Easy()
    {
        SceneManager.LoadScene("Easy");
    }
    public void Medium()
    {
        SceneManager.LoadScene("Main");
    }
    public void Hard()
    {
        SceneManager.LoadScene("Hard");
    }
}
