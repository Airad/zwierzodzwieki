﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerEasy : MonoBehaviour {

    public Animal[] animals;
    private static List<Animal> guessAnimal;
    private Animal[] currentAnimals;
    private string[] chosenAnimals;
    private AudioSource SoundSource;
    public AudioSource MusicSource;
    public AudioClip command;
    public AudioClip good;
    public AudioClip wrong;
    private int chosen;
    [SerializeField]
    private float timeBTWQuestions = 1f;
    [SerializeField]
    public Text first;
    [SerializeField]
    public Text second;
    private List<int> chosenButtons;
    void Start() {
        currentAnimals = new Animal[2];
        chosenAnimals = new string[2];
        SoundSource = GetComponent<AudioSource>();
        chosen = 0;
        if (guessAnimal == null || guessAnimal.Count < 2)
        {
            guessAnimal = animals.ToList<Animal>();
        }
        chosenButtons = new List<int>() { 0, 1 };
        SoundSource.clip = command;
        SoundSource.Play();
        SetCurrentAnimals();
	}
    void SetCurrentAnimals()
    {
        for (int i = 0; i < 2; i++)
        {
            int randomAnimal = Random.Range(0, guessAnimal.Count);
            //while(currentAnimals[randomIndex]!=null) randomIndex = Random.Range(0, 2);
            currentAnimals[i] = guessAnimal[randomAnimal];
            //Debug.Log(currentAnimals[i].sound);
            guessAnimal.RemoveAt(randomAnimal);
        }
        //Debug.Log(currentAnimals[0].animal + " " + currentAnimals[1].animal + " " + currentAnimals[2].animal);
        int index = 0;
        while (chosenButtons.Count > 0)
        {
            int randomIndex = Random.Range(0, chosenButtons.Count);
            if(index == 0) first.text = currentAnimals[chosenButtons[randomIndex]].animal;
            if (index == 1) second.text = currentAnimals[chosenButtons[randomIndex]].animal;
            chosenButtons.RemoveAt(randomIndex);
            index++;
        }
        StartCoroutine(SoundsOneByOne(currentAnimals));
    }
	void userPickAnimals()
    {
        if (chosen == 1)
        {
            isTrue();
            //StartCoroutine(TransisiontoNext());
        }
        else chosen++;
    }
    IEnumerator TransisiontoNext()
    {
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        yield return new WaitForSeconds(timeBTWQuestions);
        Debug.Log("transition");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    bool isTrue()
    {
        MusicSource.Stop();
        for (int i = 0; i < 2; i++)
        {
            if (currentAnimals[i].animal != chosenAnimals[i])
            {
                SoundSource.clip = wrong;
                SoundSource.Play();
                Debug.Log("Spróbuj jeszcze raz");
                chosen = 0;
                StartCoroutine(isMusicPlaying());
                return false;
            }

        }
        SoundSource.clip = good;
        SoundSource.Play();
        Debug.Log("Dobrze!");
        StartCoroutine(TransisiontoNext());
        return true;
    }
    public void UserPickFirst()
    {
        chosenAnimals[chosen]=first.text;
        userPickAnimals();
    }
    public void UserPickSecond()
    {
        chosenAnimals[chosen] = second.text;
        userPickAnimals();
    }
    public void RepeatSound()
    {
        StartCoroutine(SoundsOneByOne(currentAnimals));
    }

    private IEnumerator SoundsOneByOne(Animal[] animalSounds)
    {
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        for (int i = 0; i < 2; i++)
        {
            SoundSource.clip = animalSounds[i].sound;
            SoundSource.Play();

            waitFrame = new WaitForEndOfFrame();
            while (SoundSource.isPlaying)
            {
                yield return waitFrame;
            }
        }
    }
    private IEnumerator isMusicPlaying()
    {
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        Debug.Log("is playing");
        MusicSource.Play();
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
