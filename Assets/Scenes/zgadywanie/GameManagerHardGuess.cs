﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManagerHardGuess : MonoBehaviour {
    public Question[] questions;
    private static List<Question> unansweredQuestions;
    private Question current;
    private static List<Question> unusedAnimals;
    private int previousIndex;
    private AudioSource SoundSource;
    public AudioSource MusicSource;
    public AudioClip command;
    public AudioClip good;
    public AudioClip wrong;
    private string answer;
    [SerializeField]
    private Text firstText;
    [SerializeField]
    private Text secondText;
    [SerializeField]
    private Text thirdText;
    [SerializeField]
    private Text forthText;
    [SerializeField]
    private float timeBTWQuestions = 1f;
    [SerializeField]
    //private Animator animator;
    private static List<int> buttons = new List<int> { 0, 1, 2, 3 };
    private static List<int> whichButton;

    void Start()
    {
        SoundSource = GetComponent<AudioSource>();
        if (unansweredQuestions == null || unansweredQuestions.Count<4)
        {
            unansweredQuestions = questions.ToList<Question>();
        }
        if (whichButton == null || whichButton.Count == 0)
        {
            whichButton = buttons.ToList<int>();
            unusedAnimals = questions.ToList<Question>();
        }
        
        SetCurrentQuestion();
    }
    void SetCurrentQuestion()
    {
        int randomQuestionIndex = Random.Range(0, unansweredQuestions.Count);
        current = unansweredQuestions[randomQuestionIndex];
        //Debug.Log(unansweredQuestions[0].sound.name);
        
        int randomButtonIndex = Random.Range(0, 3);
        answer = randomButtonIndex.ToString();
        StartCoroutine(isPlaying());
        //factText.text = current.fact;
        Debug.Log("unused" + unusedAnimals.Count);
        unansweredQuestions.Remove(current);
        
        SetButton();
    }
    public void SetButton()
    {
        unusedAnimals.Remove(current);
        Debug.Log("unused"+ unusedAnimals.Count);
        if (answer == "0")
        {
            firstText.text = current.animal;
            Debug.Log(whichButton[0]);
            whichButton.RemoveAt(0);
        }
        else if (answer == "1")
        {
            secondText.text = current.animal;
            Debug.Log(whichButton[1]);
            whichButton.RemoveAt(1);
        }
        else if (answer == "2")
        {
            thirdText.text = current.animal;
            Debug.Log(whichButton[2]);
            whichButton.RemoveAt(2);
        }
        else if (answer == "3")
        {
            forthText.text = current.animal;
            Debug.Log(whichButton[2]);
            whichButton.RemoveAt(2);
        }
        while (whichButton.Count != 0)
        {
            int randomQuestionIndex = Random.Range(0, unusedAnimals.Count);
            switch (whichButton[0])
            {
                case 0:
                    Debug.Log(randomQuestionIndex + " " + unusedAnimals.Count);
                    firstText.text = unusedAnimals[randomQuestionIndex].animal;
                    unusedAnimals.RemoveAt(randomQuestionIndex);
                    break;
                case 1:
                    Debug.Log(randomQuestionIndex+" "+unusedAnimals.Count);
                    secondText.text = unusedAnimals[randomQuestionIndex].animal;
                    unusedAnimals.RemoveAt(randomQuestionIndex);
                    break;
                case 2:
                    Debug.Log(randomQuestionIndex + " " + unusedAnimals.Count);
                    thirdText.text = unusedAnimals[randomQuestionIndex].animal;
                    unusedAnimals.RemoveAt(randomQuestionIndex);
                    break;
                case 3:
                    Debug.Log(randomQuestionIndex + " " + unusedAnimals.Count);
                    forthText.text = unusedAnimals[randomQuestionIndex].animal;
                    unusedAnimals.RemoveAt(randomQuestionIndex);
                    break;
            }
            //Debug.Log(randomQuestionIndex+ " "+firstText.text + " " + secondText.text + " " + thirdText.text);
            whichButton.RemoveAt(0);
        }
    }
    IEnumerator TransisiontoNext()
    {
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        yield return new WaitForSeconds(timeBTWQuestions);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void UserSelectFirstButton()
    {
        if (answer == "0")
        {
            MusicSource.Stop();
            SoundSource.clip = good;
            SoundSource.Play();
            StartCoroutine(TransisiontoNext());
        }
        else
        {
            MusicSource.Stop();
            SoundSource.clip = wrong;
            SoundSource.Play();
            Debug.Log("Źle!");
            StartCoroutine(isMusicPlaying());
        }
        //StartCoroutine(TransisiontoNext());
    }
    public void UserSelectSecondButton()
    {
        if (answer == "1")
        {
            MusicSource.Stop();
            SoundSource.clip = good;
            SoundSource.Play();
            StartCoroutine(TransisiontoNext());
        }
        else
        {
            MusicSource.Stop();
            SoundSource.clip = wrong;
            SoundSource.Play();
            Debug.Log("Źle!");
            StartCoroutine(isMusicPlaying());
        }
        //StartCoroutine(TransisiontoNext());
    }
    public void UserSelectThirdButton()
    {
        if (answer == "2")
        {
            MusicSource.Stop();
            SoundSource.clip = good;
            SoundSource.Play();
            StartCoroutine(TransisiontoNext());
        }
        else
        {
            MusicSource.Stop();
            SoundSource.clip = wrong;
            SoundSource.Play();
            Debug.Log("Źle!");
            StartCoroutine(isMusicPlaying());
        }
        //StartCoroutine(TransisiontoNext());
    }
    public void UserSelectForthButton()
    {
        if (answer == "3")
        {
            MusicSource.Stop();
            SoundSource.clip = good;
            SoundSource.Play();
            StartCoroutine(TransisiontoNext());
        }
        else
        {
            MusicSource.Stop();
            SoundSource.clip = wrong;
            SoundSource.Play();
            Debug.Log("Źle!");
            StartCoroutine(isMusicPlaying());
        }
    }
    private IEnumerator isPlaying()
    {
        SoundSource.clip = command;
        SoundSource.Play();
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        Debug.Log("is playing");
        SoundSource.clip = current.sound;
        SoundSource.Play();
        waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }

    }
    private IEnumerator isMusicPlaying()
    {
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        Debug.Log("is playing");
        MusicSource.Play();
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
