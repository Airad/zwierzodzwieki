﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManagerGuess : MonoBehaviour {
    public Question[] questions;
    private static List<Question> unansweredQuestions;
    private Question current;
    private AudioSource SoundSource;
    public AudioSource MusicSource;
    public AudioClip command;
    public AudioClip good;
    public AudioClip wrong;
    [SerializeField]
    private Text trueText;
    [SerializeField]
    private Text falseText;
    [SerializeField]
    private float timeBTWQuestions = 1f;
    [SerializeField]
    private Text trueAnswerText;
    [SerializeField]
    private Text falseAnswerText;
    [SerializeField]
    //private Animator animator;

    void Start()
    {
        SoundSource = GetComponent<AudioSource>();
        if (unansweredQuestions == null || unansweredQuestions.Count==0)
        {
            unansweredQuestions = questions.ToList<Question>();
        }
        
        SetCurrentQuestion();
    }
    void SetCurrentQuestion()
    {
        int randomQuestionIndex = Random.Range(0, unansweredQuestions.Count);
        current = unansweredQuestions[randomQuestionIndex];
        //Debug.Log(unansweredQuestions[0].sound.name);
        //factText.text = current.fact;
        StartCoroutine(isPlaying());
        if (current.isTrue)
        {
            trueAnswerText.text = "Dobrze!";
            falseAnswerText.text = "Źle";
        }
        else
        {
            trueAnswerText.text = "Źle";
            falseAnswerText.text = "Dobrze!";
        }
        SetButton();
    }
    public void SetButton()
    {
        if (current.isTrue)
        {
            trueText.text = current.animal;
            int randomAnimalIndex= Random.Range(0, unansweredQuestions.Count);
            if (trueText.text == unansweredQuestions[randomAnimalIndex].animal) falseText.text = unansweredQuestions[(randomAnimalIndex + 1) % unansweredQuestions.Count].animal;
            else falseText.text = unansweredQuestions[randomAnimalIndex].animal;
        }
        else
        {
            falseText.text = current.animal;
            int randomAnimalIndex = Random.Range(0, unansweredQuestions.Count);
            if (falseText.text == unansweredQuestions[randomAnimalIndex].animal) trueText.text = unansweredQuestions[(randomAnimalIndex+1)%unansweredQuestions.Count].animal;
            else trueText.text = unansweredQuestions[randomAnimalIndex].animal;
        }
        Debug.Log(trueText.text + " " + falseText.text);
    }
    IEnumerator TransisiontoNext()
    {
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        unansweredQuestions.Remove(current);

        yield return new WaitForSeconds(timeBTWQuestions);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void UserSelectTrue()
    {
        //animator.SetTrigger("True");
        if (current.isTrue)
        {
            MusicSource.Stop();
            SoundSource.clip = good;
            SoundSource.Play();
            Debug.Log("Correct");
            StartCoroutine(TransisiontoNext());
        }
        else
        {
            MusicSource.Stop();
            SoundSource.clip = wrong;
            SoundSource.Play();
            Debug.Log("Wrong");
            StartCoroutine(isMusicPlaying());
        }
        //StartCoroutine(TransisiontoNext());
    }
    public void UserSelectFalse()
    {
        //animator.SetTrigger("False");
        if (current.isTrue)
        {
            MusicSource.Stop();
            SoundSource.clip = wrong;
            SoundSource.Play();
            Debug.Log("Wrong");
            StartCoroutine(isMusicPlaying());
        }
        else
        {
            MusicSource.Stop();
            SoundSource.clip = good;
            SoundSource.Play();
            Debug.Log("Correct");
            StartCoroutine(TransisiontoNext());
        }   
    }
    private IEnumerator isPlaying()
    {
        SoundSource.clip = command;
        SoundSource.Play();
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        Debug.Log("is playing");
        SoundSource.clip = current.sound;
        SoundSource.Play();
        waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
       
    }
    private IEnumerator isMusicPlaying()
    {
        WaitForEndOfFrame waitFrame = new WaitForEndOfFrame();
        while (SoundSource.isPlaying)
        {
            yield return waitFrame;
        }
        Debug.Log("is playing");
        MusicSource.Play();
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
