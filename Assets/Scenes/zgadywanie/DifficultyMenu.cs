﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DifficultyMenu : MonoBehaviour
{
    public void EasyModePlay()
    {
        SceneManager.LoadScene("Main");
    }
    public void GoBack()
    {
        Debug.Log("Get Back!");
        SceneManager.LoadScene("MainMenu");
    }
    public void NormalModePlay()
    {
        Debug.Log("Normal");
        SceneManager.LoadScene("Normal");
    }
    public void HardModePlay()
    {
        Debug.Log("Hard");
        SceneManager.LoadScene("Hard");
    }
}
