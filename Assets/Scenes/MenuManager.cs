﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
    
    private int chosen;

    public void PlayGuess()
    {
        chosen = 1;
    }
    public void PlayOrder()
    {
        chosen = 2;
    }
    public void PlayVerticalPuzzle()
    {
        chosen = 3;
    }
    public void PlaySquarePuzzle()
    {
        chosen = 4;
    }
    public void PlayHide()
    {
        SceneManager.LoadScene("SampleScene"); 
    }
    public void EasySelect()
    {
        switch (chosen)
        {
            case 1:
                TransitionToGame("EasyGuess");
                break;
            case 2:
                TransitionToGame("EasyOrder");
                break;
            case 3:
                TransitionToGame("veasy");
                break;
            case 4:
                TransitionToGame("EasyPuzzleSquare");
                break;
        }
    }
    public void MediumSelect()
    {
        switch (chosen)
        {
            case 1:
                TransitionToGame("NormalGuess");
                break;
            case 2:
                TransitionToGame("MediumOrder");
                break;
            case 3:
                TransitionToGame("vmedium");
                break;
            case 4:
                TransitionToGame("MediumPuzzleSquare");
                break;
        }
    }
    public void HardSelect()
    {
        switch (chosen)
        {
            case 1:
                TransitionToGame("HardGuess");
                break;
            case 2:
                TransitionToGame("HardOrder");
                break;
            case 3:
                TransitionToGame("vhard");
                break;
            case 4:
                TransitionToGame("HardPuzzleSquare");
                break;
        }
    }
    void TransitionToGame(string name)
    {
        SceneManager.LoadScene(name);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
}
